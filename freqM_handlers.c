/**
 * Sledge - wide functionality library, suitable for embedded systems, 
 * mainly for ARM Cortex and more particularly for STM32 family
 * 
 * Copyright (c) 2015, Ivan Kuvaldin. All rights reserved.
 * 
 * Licensed under BSD or MIT. 
 * Please refer to Licence.BSD.txt or Licence.MIT.txt provided with these sources.
 * You may obtain a copy of the Licenses at
 *      http://opensource.org/licenses/bsd-license.php  
 *      http://opensource.org/licenses/mit-license.php  
 */
/**
 * @author  Ivan Кувалда(Sledgehammer) Kuvaldin <mailto:i.kyb@ya.ru>
 * @brief   
 *          
 *			
 */

#include "./freqM.h"
#include "Sledge/bsp.h"

///
extern freqM_Callback_t freqM_ready_callback;
///
extern void *freqM_callback_params;
	
/*/// Переменные, сохраняющие в памяти значения соответствующих регистров таймеров. Получено по DMA
extern volatile uint16_t CCR1mem, 
						 CNT4mem;

/// Тоже, но массив, для отладки
extern volatile uint16_t CCR1memArr[],
						 CNT4memArr[];
*/
// Коэффициенты поправки для вычисления частоты
//volatile uint32_t freqM_k_ARR = 0;
//volatile uint32_t freqM_k_RCR = 1;

/// Коэффициент поправки для вычисления частоты
static volatile int32_t k_straight = 2;
static volatile int32_t k_reci = 0;




/** 
 * Прошел интервал измерения. Посчитать частоту. 
 * Если используется обычный режим счета (defaultMEASUREMENT_MODE==1)
 * @note В данной версии плохо считает частоту кратную частоте дискретизации?
 */
///\note неизвестно как оно работает еперь после очень многих изменений не тестировалось
/*extern "C" void freqM_RefTimer_Upd_IRQHandler(void) 
{
	double freq;
	uint32_t inPulses, refPulses;
	
//	TIM1->EGR = TIM_EGR_UG;
//	TIM4->EGR = TIM_EGR_UG;
	
	inPulses = TIM4->CNT;//CNT4mem;	// * (TIM12->CNT+1);
	refPulses = TIM1->CCR1 - k_straight;	//CCR1mem				/// -k на синхронный старт по триггеру
				//- hwInputFilter *2; 	/// -10ns на каждую доп. единицу фильтра
	freq = ( (double) inPulses ) / refPulses * freqM_timfreqbase * (1<<freqM_hwInputPrescaler);
	
	freq /= 1; //Dummy
	
	//TIM1->EGR = TIM_EGR_UG;
	TIM4->EGR = TIM_EGR_UG;
}*/

/// Количество перепонений опорного таймера. Для программной реализации. 
static volatile uint32_t ref_tim_overflows = 0;
/** 
 * Eсли update произошёл до того как пришёл последний фронт, 
 * значит частота слишком низкая, чтоб её посчитать при такой конфигураци.
 * \todo определить что  частота слишком низкая
 */
void freqM_RefTimer_Upd_IRQHandler(void) 
{
	if( TIM_GetITStatus( freqM_settings.reftimer->tim, TIM_IT_Update) ){
		TIM_ClearITPendingBit( freqM_settings.reftimer->tim, TIM_IT_Update ); //freqM_RefTimer->tim->DIER &= ~TIM_DIER_UIE;
		++ref_tim_overflows;
		if( ref_tim_overflows == (uint32_t)-1 ){  //UINT32_MAX 
			// Остановить измерение
			freqM_settings.reftimer->tim->CR1 &= ~TIM_CR1_CEN;
			freqM_settings.counter ->tim->CR1 &= ~TIM_CR1_CEN;
			
			// отчитаться
			//if( freqM_settings.callback.func )
				//freqM_settings.callback.func(0, freqM_settings.callback.params); // частота ниже, чем способны определить, выдать 0
			if( freqM_settings.fail_callback.func )
				freqM_settings.fail_callback.func(freqM_settings.fail_callback.params); // частота ниже, чем способны определить
			
			// и начать всё с начала
			freqM_settings.counter->tim->SMCR |= TIM_SlaveMode_Trigger;	// Включить slave mode счетчика внешних импульсов
		}
	}
}


static volatile double freq;
static volatile uint32_t inPulses, refPulses;
/** 
 * Прошел интервал измерения и дождались следующего фронта. Посчитать частоту. 
 * Должна быть вызвана в timRef_СC_IRQHandler() (напр. TIM1_CC_IRQHandler() ),
 * Если используется обратный (reciprocal) режим счета (defaultMEASUREMENT_MODE==2)
 */
void freqM_RefTimer_CC_IRQHandler() 
{
	//double freq;
	//uint32_t inPulses, refPulses;
	
	// срочно закэшировать значения регистров, хотя в принципе после останоки они не изменятся
	inPulses = freqM_settings.counter->tim->CNT;
	//refPulses = freqM_RefTimer->tim->CCR1 + freqM_RefTimer->tim->ARR+1 - freqM_k;
	refPulses = freqM_settings.reftimer->tim->CCR1 + (freqM_settings.reftimer->tim->ARR+1)*ref_tim_overflows - k_reci;
	
	// Остановить таймеры.
	freqM_settings.reftimer  ->tim->CR1 &= ~TIM_CR1_CEN;
	freqM_settings.counter ->tim->CR1 &= ~TIM_CR1_CEN;
	// отключить автостарт, будет запущен в прерывании от sample_rate таймера, или если не нужно точной дискретизации после окончания расчётов
	freqM_settings.counter ->tim->SMCR &= ~TIM_SMCR_SMS;	// Отключить slave mode у счетчика внешних импульсов.
	
	// Сбросить флаг, запретить прерывание.
	freqM_settings.reftimer->tim->SR &= ~TIM_SR_CC1IF;		// Сбросить флаг прерывания CC1 опорного таймера
	freqM_settings.reftimer->tim->DIER &= ~TIM_DIER_CC1IE;	// Запретить прерывание CC1 опорного таймера. Будет разрешено позже по DMA.
	// Сбросить счетные регистры таймеров (Сгенерировать программно событие Update)
	freqM_settings.reftimer  ->tim->EGR = TIM_EGR_UG;
	freqM_settings.counter ->tim->EGR = TIM_EGR_UG;
	// Можно работать дальше - включить автостарт
	
	// частота =      частота опор.тактов            / к-во опор.тактов * к-во вх. импульсов * коэф.предделителя
	freq = (double)freqM_settings.reftimer_base_freq / refPulses        * inPulses           * (1<<freqM_settings.hwInputPrescaler);
	ref_tim_overflows = 0;
	
//	if( freqM_ready_callback.func )
//		freqM_ready_callback.func(freq, freqM_ready_callback.params);
	if( freqM_settings.callback.func )
		freqM_settings.callback.func(freq, freqM_settings.callback.params);
	
	freqM_settings.counter->tim->SMCR |= TIM_SlaveMode_Trigger;	// Включить slave mode счетчика внешних импульсов
}


/// Обработчик прерывания таймера, задающего частоту дискретизации
void freqM_SampleRateTimer_IRQHandler()
{
	if( TIM_GetITStatus( freqM_settings.samplertimer->tim, TIM_IT_Update) ){
		TIM_ClearITPendingBit( freqM_settings.samplertimer->tim, TIM_IT_Update );
		// Сбросить счетные регистры и остановить таймеры. По идее уже сброшены
		freqM_settings.reftimer->tim->EGR = TIM_EGR_UG;	
		freqM_settings.counter ->tim->EGR = TIM_EGR_UG; 	
		// Можно работать дальше
		// Удостовериться что мы не прервали измерений раньше времени -> слишком низкая частота
		assert_msg("Sample rate timer's IRQ waken before extra window ended with last input front.\n", 
			(freqM_settings.counter->tim->SMCR & TIM_SMCR_SMS) == 0 
		);
		freqM_settings.counter->tim->SMCR |= TIM_SlaveMode_Trigger;	// Включить slave mode счетчика внешних импульсов. @note Должно быть отключено до того
	}
}

